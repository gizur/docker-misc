#!/bin/sh

# Size of webapp.tar file fo php_applications is more that 185 MB 
# GitHUB dies not allow more than 100.
# php_applications and gizursaas_v2 must be in same dir.

CDIR=$PWD
echo "In gizursaas-v2: $CDIR"

# Store the docker0 interface id for redis-dns
# This will help to resolve hostname in the containers to connect with services
# running at Linux Server (Vagrant Host) 
IP=$(ip -o -4 addr list docker0 | perl -n -e 'if (m{inet\s([\d\.]+)\/\d+\s}xms) { print $1 }')
echo "ifconfig docker0 IP : $IP\n"

echo "Building Redis Container: gizur/redis"
cd $CDIR && git pull
cd $CDIR/containers/redis && docker build -t gizur/redis .
REDISID=$(docker run -d -p 6379 -dns $IP -dns 8.8.8.8 -dns 8.8.4.4 gizur/redis)
REDIS_IP=$(docker inspect $REDISID | grep IPAddress | cut -d '"' -f 4)

echo "Building Apache Container: gizur/apache2"
cd $CDIR/../php_applications && git pull && git checkout docker-migration
cd $CDIR/../php_applications/instance-configuration && ./deploy-configuration gc3-docker
cd $CDIR/../php_applications && docker build -t gizur/apache2 .
APACHEID=$(docker run -d -p 80 -dns $IP -dns 8.8.8.8 -dns 8.8.4.4 gizur/apache2)
APACHE_IP=$(docker inspect $APACHEID | grep IPAddress | cut -d '"' -f 4)

echo "Building MySQL Container: gizur/mysql"
cd $CDIR && git pull
cd $CDIR/containers/mysqld && docker build -t gizur/mysql .
MYSQLID=$(docker run -d -p 3306 -dns $IP -dns 8.8.8.8 -dns 8.8.4.4 gizur/mysql)
MYSQL_IP=$(docker inspect $MYSQLID | grep IPAddress | cut -d '"' -f 4)

# Update redis
# Add one row in GIZUR_ACCOUNTS table
redis-cli -h $REDIS_IP hmset GIZUR_ACCOUNTS:cikab name_1 Cikab id cikab@gizur.com status Active databasename vtiger_cikab_d_a7902d94 dbpassword "mysql-server" server db.gizur.local port 3306 username admin id_sequence 1000 clientid cikab

# Update redis-dns and add current system IP to 
# resolve redis.gizur.local

echo "RUNNING: redis-cli -h $IP lpush frontend:app.gizur.local http://$APACHE_IP:80"
redis-cli -h $IP lpush frontend:app.gizur.local http://$APACHE_IP:80

echo "RUNNING: redis-cli -h $IP set redis-dns:redis.gizur.local $REDIS_IP"
redis-cli -h $IP set redis-dns:redis.gizur.local $IP

echo "RUNNING: redis-cli -h $IP set redis-dns:app.gizur.local $APACHE_IP"
redis-cli -h $IP set redis-dns:app.gizur.local $APACHE_IP

echo "RUNNING: redis-cli -h $IP set redis-dns:db.gizur.local $MYSQL_IP"
redis-cli -h $IP set redis-dns:db.gizur.local $MYSQL_IP

echo "End of init script"
