## Migration to Docker Container

### Folder Structure

```
app       --->       webapp.tar having full php_applications files and folders with Dockerfile
mysql     --->       backups
          --->       Dockerfile
          --->       start.sh
nodejs    --->       Dockerfile with full nodejs_applications files and folders
redis     --->       Dockerfile
```

### Introduction

### Issues

* backups folder of mysql will have more that one sql dump. need to figureout how to know files database name, user, password and other details.

### Migration
This folder has import.js which on run update the redis server with GIZUR_ACCOUNTS dynamodb data.

# Init.sh

## Pre-configuration before running init.sh

* get ip of docker0 by ``` ifconfig docker0 ``` (Here 0 in docker0 may differ in your system)
* Update redis config to run redis on docker0 ip (bind docker0_ip)
* Update redis IP in hipatche config file 
* Update redis IP in redis-dns config file
* Run redis & hipatche as normal user and redis-dns as superuser

## Code repositories

* php_applications, gizursaas-v2 and jacc-dev (https://github.com/gizur-ess-prabhat/jacc-dev) must be in one folder (My folder name is app.)
ls output is

```
vagrant@gizurapp:~/app$ ls
gizursaas-v2  php_applications

```

* Update code by the following commands

```

vagrant@gizurapp:~/app/php_applications$ git pull
vagrant@gizurapp:~/app/php_applications$ git checkout origin docker-migration
vagrant@gizurapp:~/app/php_applications$ cd instance-configuration/
vagrant@gizurapp:~/app/php_applications/instance-configuration$ ./deploy-configuration gc3-docker
vagrant@gizurapp:~/app/php_applications/instance-configuration$ cd ..
vagrant@gizurapp:~/app/php_applications$ docker build .


vagrant@gizurapp:~/app/gizursaas-v2$ git pull

```

### if your app folder name is different update init.sh (Line 53 & 57)

Run init.sh

### Add app.gizur.local in your host computer /etc/hosts file (In Mac the location may be different)

Open browser 
* http://app.gizur.local:8080/ - Gizur SaaS
* http://app.gizur.local:8080/cikab/ - vtiger (admin / frefug4staY7)


