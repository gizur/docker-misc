//------------------------------
//
// 2012-08-28, Prabhat Khera
//
// Copyright Gizur AB 2012
//
// Install with dependencies: npm install 
//
// Documentation is 'docco style' - http://jashkenas.github.com/docco/
//
// Using Google JavaScript Style Guide - http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
//------------------------------


"use strict";

(function() {
    //Create AWS object and set config
    var AWS = require('aws-sdk');
    var fs = require('fs');
    var redis = require("redis");

    var config = require('./config.js').Config;
    
    // Delete the redis entries
    //
    var redis_client = redis.createClient(config.redisPort, config.redisHostname, config.redisOptions);

    redis_client.on("connect", function() {
        console.log("Saving in redis : ")
        redis_client.del('gizur-accounts', function(err) {
            if (err) {
                console.log('Error deleting redis entries : ' + err);
            }
        });

        redis_client.quit();
    }.bind(this));

    // redis error management
    redis_client.on("error", function(err) {
        console.log("Redis error: " + err);
        redis_client.quit();
    });


    // Configure AWS
    //
    AWS.config.update({
        accessKeyId: config.AWS_API_KEY,
        secretAccessKey: config.AWS_SECRET_KEY,
        region: "eu-west-1"
    });

    //Create DynamoDB Object and set config
    var db = new AWS.DynamoDB();
    var params = {
        "TableName": "GIZUR_ACCOUNTS"
    };

    db.scan(params, function(err, data) {

        if (data !== null) {
            var redis_client = redis.createClient(config.redisPort, config.redisHostname, config.redisOptions);

            redis_client.on("connect", function() {

                for (var index in data.Items) {
                    var dataRow = data.Items[index];

                    //console.log(dataRow.clientid['S'] + " : " + JSON.stringify(dataRow.id['S']));
                    var id = dataRow.id['S'];
                    
                    for(var key in dataRow) {
                        if (dataRow.hasOwnProperty(key)) {
                            
                            console.log(key + " : " + JSON.stringify(dataRow[key]));
                            redis_client.hmset('GIZUR_ACCOUNTS:' + dataRow.clientid['S'], key, dataRow[key].S, function(err, res) {
                                if (err) {
                                    console.log('Error updating redis : (' + err + ")");
                                } else {
                                    console.log('Response : ' + res);
                                }
                            });
                        }
                    }
                }

                redis_client.quit();
            }.bind(this));
        } else
            console.log("DynamoDB is empty." + err);
    });
}());
