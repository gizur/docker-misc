<?php
/**
 * @category   Cronjobs
 * @package    Integration
 * @subpackage CronJob
 * @author     Prabhat Khera <gizur-ess-prabhat@gizur.com>
 * @version    SVN: $Id$
 * @link       href="http://gizur.com"
 * @license    Commercial license
 * @copyright  Copyright (c) 2012, Gizur AB, 
 * <a href="http://gizur.com">Gizur Consulting</a>, All rights reserved.
 *
 * purpose : Connect to Amazon SQS through aws-php-sdk
 * Coding standards:
 * http://pear.php.net/manual/en/standards.php
 *
 * PHP version 5.3
 *
 */

ini_set('display_errors', 'On');
error_reporting(E_ALL);
$output = null;

function runbatch($file){
    $sudo = "sudo";
    if(isset($_GET['issudo']))
        $sudo = '';
    
    @shell_exec($sudo . "chmod +x " . __DIR__ . "/$file");
    $output = shell_exec($sudo . __DIR__ . "/$file");
    @shell_exec($sudo . 'chmod -x ' . __DIR__ . "/$file");
    return $output;
}

if (isset($_GET['action'])) {
    
    switch ($_GET['action']) {
        case 'setup-tables':            
            $output = runbatch('setup-tables.sh');
            break;
        case 'phpcronjob1':
            $output = runbatch('phpcronjob1.sh');
            break;
        case 'phpcronjob2':
            $output = runbatch('phpcronjob2.sh');
            break;
        case 'phpcronjob3':
            $output = runbatch('phpcronjob3.sh');
            break;
        case 'phpcronjobs':
            /*
             * Cron Job 1
             */
            $output = runbatch('phpcronjob1.sh');
            /*
             * Cron Job 2
             */
            $output .= runbatch('phpcronjob2.sh');
            /*
             * Cron Job 3
             */
            $output .= runbatch('phpcronjob3.sh');
            break;
        case 'mail_report':  
            $output = runbatch('../reports/sales_orders.sh');
            break;
        case 'mail_report_csv':  
            $output = runbatch('../reports/sales_orders_csv.sh');
            break;
        case 'phpinfo':
            phpinfo();
            break;
    }
    
    echo $output;
}
