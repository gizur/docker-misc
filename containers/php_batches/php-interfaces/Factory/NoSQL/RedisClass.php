<?php

include '../../config.inc.php';
require 'Predis/Autoloader.php';

class RedisClass implements NoSQLInterface
{

    private $redis = null;

    function __construct()
    {
        Predis\Autoloader::register();
        $this->redis = new Predis\Client(array(
            'scheme' => 'tcp',
            'host' => '127.0.0.1',
            'port' => 6379,
        ));
    }

    public function get($key)
    {
        
    }

    public function set($key, $value)
    {
        
    }

}

?>