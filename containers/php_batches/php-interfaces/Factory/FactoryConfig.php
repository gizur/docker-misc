<?php

class FactoryConfig
{

    public static $driversInUse = array(
        'Cache' => "Redis", // Memcache
        'NoSQL' => "Redis", // AmazonDynamoDB
        'Queue' => "RabbitMQ", // AmazonSQS
        'MySQL' => "MySQL", // RDS
    );

}

?>