<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QueueTest
 *
 * @author prabhat
 */
include '../Factory/QueueFactory.php';

class QueueTest
{
    private $queue;
    
    public function __construct()
    {
        $factory = new QueueFactory();
        $this->queue = $factory->getInstance();
    }
    
    public function addMessage() {
        $this->queue->publishMessage("SET_FILES", "Hello " . rand() . " !!!");
    }
    
    public function getMessages() {
        print_r($this->queue->getAllMessages("SET_FILES"));
    }
}


$test = new QueueTest();
$test->addMessage();
$test->addMessage();
$test->addMessage();

$test->getMessages();
?>
