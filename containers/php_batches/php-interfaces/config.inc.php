<?php

/**
 * This file contains common functions used throughout the Integration package.
 *
 * @package    Integration
 * @subpackage Config
 * @author     Jonas Colmsjö <jonas.colmsjo@gizur.com>
 * @version    SVN: $Id$
 *
 * @license    Commercial license
 * @copyright  Copyright (c) 2012, Gizur AB, 
 * <a href="http://gizur.com">Gizur Consulting</a>, All rights reserved.
 *
 * Coding standards:
 * http://pear.php.net/manual/en/standards.php
 *
 * PHP version 5
 *
 */

include __DIR__. '/Factory/FactoryConfig.php';

class Config extends FactoryConfig
{
    public static $dbIntegration = array(
        "RDS" => array(
            'db_server' => 'gizurcloud.colm85rhpnd4.eu-west-1.rds.amazonaws.com',
            'db_port' => 3306,
            'db_username' => 'vtiger_integrati',
            'db_password' => 'ALaXEryCwSFyW5jQ',
            'db_name' => 'vtiger_integration',
            'db_type' => 'mysql'),
        "MySQL" => array(
            'db_server' => 'db.gizur.local',
            'db_port' => 3306,
            'db_username' => 'admin',
            'db_password' => 'mysql-server',
            'db_name' => 'vtiger_integration',
            'db_type' => 'mysql')
    );
    public static $dbVtiger = array(
        "RDS" => array(
            'db_server' => 'gizurcloud.colm85rhpnd4.eu-west-1.rds.amazonaws.com',
            'db_port' => 3306,
            'db_username' => 'user_6bd70dc3',
            'db_password' => 'fbd70dc30c05',
            'db_name' => 'vtiger_7cd70dc3',
            'db_type' => 'mysql'),
        "MySQL" => array(
            'db_server' => 'db.gizur.local',
            'db_port' => 3306,
            'db_username' => 'admin',
            'db_password' => 'mysql-server',
            'db_name' => 'vtiger_cikab_d_a7902d94',
            'db_type' => 'mysql')
    );
    public static $batchVariable = 99;
    public static $setFtp = array(
        'host' => "10.58.226.192",
        'port' => 21,
        'username' => "gizur",
        'password' => "gizur",
        'serverpath' => "files/"
    );
    public static $mosFtp = array(
        'host' => "10.58.226.192",
        'port' => 21,
        'username' => "gizur",
        'password' => "gizur",
        'serverpath' => "files/"
    );
    public static $amazonQ = array(
        "AmazonSQS" => array(
            'url' => 'https://sqs.eu-west-1.amazonaws.com/065717488322/cikab_queue'),
        "RabbitMQ" => array(
            'url' => 'queue.gizur.local',
            'port' => 5672,
            'username' => 'guest',
            'password' => 'guest',
            'vhost' => '/',
            'AMQP_DEBUG' => true
        )
    );
    public static $amazonSThree = array(
        'setBucket' => "gc3-archive",
        'setFolder' => "seasonportal/SET-files/",
        'mosBucket' => "gc3-archive",
        'mosFolder' => "seasonportal/SET-files/"
    );
    public static $customFields = array(
        'setFiles' => 'cf_650',
        'mosFiles' => 'cf_651',
        'basProductId' => 'cf_652'
    );
    public static $lineBreak = "\r\n";
    public static $toEmailReports = array(
        "prabhat.khera@essindia.co.in"
    );

    public static function getDriver($driver){
        return parent::$driversInUse[$driver];
    }
}
