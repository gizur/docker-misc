#!/bin/bash

#/usr/bin/mysqld_safe &
/usr/sbin/mysqld &
sleep 2

dbUser=root
dbPassword=mysql-server

mysqladmin -u $dbUser password $dbPassword

for FILE in `ls /src/backups/ | egrep '\.sql$'`
do
  dbName="${FILE%%.*}"
  echo "CREATING DATABASE : $dbName"
  mysql -u$dbUser -p$dbPassword -e "CREATE DATABASE IF NOT EXISTS $dbName"
  echo "IMPORTING /src/backups/$FILE to DATABASE $dbName"
  mysql -u$dbUser -p$dbPassword $dbName < /src/backups/$FILE
done

/usr/bin/tail -f /var/log/mysql.log
