-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: gizurcloud.colm85rhpnd4.eu-west-1.rds.amazonaws.com
-- Generation Time: Sep 10, 2013 at 08:42 AM
-- Server version: 5.5.27
-- PHP Version: 5.3.27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vtiger_integration`
--
USE `vtiger_integration`;
-- --------------------------------------------------------

--
-- Table structure for table `sales_orders`
--

CREATE TABLE IF NOT EXISTS `sales_orders` (
  `id` int(19) NOT NULL AUTO_INCREMENT,
  `salesorder_no` varchar(100) DEFAULT NULL,
  `salesorderid` int(19) NOT NULL DEFAULT '0',
  `contactid` int(19) DEFAULT NULL,
  `accountname` varchar(100) DEFAULT NULL,
  `accountid` int(19) DEFAULT NULL,
  `batchno` varchar(20) NOT NULL,
  `set` enum('Yes','No') DEFAULT 'Yes',
  `mos` enum('Yes','No') DEFAULT 'No',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `duedate` date DEFAULT NULL,
  `set_status` varchar(40) DEFAULT NULL,
  `mos_status` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`salesorder_no`),
  KEY `index3` (`salesorderid`),
  KEY `index4` (`contactid`),
  KEY `index5` (`accountname`),
  KEY `index6` (`accountid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_products`
--

CREATE TABLE IF NOT EXISTS `sales_order_products` (
  `id` int(19) NOT NULL AUTO_INCREMENT,
  `productname` varchar(100) DEFAULT NULL,
  `productid` int(11) DEFAULT NULL,
  `productquantity` int(5) DEFAULT NULL,
  `featurdate` date DEFAULT NULL,
  `sales_order_id` int(19) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `bas_product_id` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`productname`),
  KEY `index3` (`productid`),
  KEY `index4` (`productquantity`),
  KEY `index5` (`sales_order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `sales_order_products`
--
ALTER TABLE `sales_order_products`
  ADD CONSTRAINT `fk_sales_order_products_1` FOREIGN KEY (`sales_order_id`) REFERENCES `sales_orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
